# [SummerDocker JDK8/JDK17](https://gitee.com/a-xin-2299/SummerDocker.git)

#### [SummerCloud](https://gitee.com/a-xin-2299/SummerCloud.git)使用组件部署文档


- **DATA:** docker镜像挂载的数据目录
- **CONF:** 镜像使用的配置文件
- **LOGS:** 镜像日志文件挂载目录


#### 1.SummerDocker

- ***start.sh: 用于启动组件的docker镜像脚本***
- ***docker-compose.yml: 用于配置三方组件docker配置项***

#### 2.build

- ***sentinel：***
  - jar: sentinel熔断限流组件1.8版本jar包
  - dockerfile:  将sentinel的jar包文件打包成docker镜像并推送至docker镜像仓库
  - sentinel.sh: 用于构建sentinel的docker镜像脚本

- ***xxlJob：***
  - jar: 分布式定时任务框架基础jar包
  - dockerfile:  将xxljob的jar包文件打包成docker镜像并推送至docker镜像仓库
  - xxljob.sh: 用于构建xxljob的docker镜像脚本

- ***snail_job：***
  - jar: 分布式定时任务框架基础jar包
  - dockerfile:  将snail_job的jar包文件打包成docker镜像并推送至docker镜像仓库
  - summer_job.sh: 用于构建snail_job的docker镜像脚本
  
- ***server：***
    - dockerfile:  微服务打包docker使用文件

#### 3.elk

- ***elasticsearch:*** elasticsearch挂载信息
- ***kibana:*** kibana挂载信息
- ***logstash:*** logstash挂载信息
- ***zipkin:*** zipkin挂载信息
- ***start.sh:*** 启动部署elk脚本
- ***docker-compose.yml:*** elk部署镜像配置和zipkin镜像配置

#### 4.启动流程

- ***1:安装docker*** 
  - 在宿主机安装docker，可参考[linux安装docker脚本](./build/install_docker.sh)
  
- ***2:启动数据源*** 
  - 预先启动mysql和pgsql服务，在本项目根目录执行：
  ```shell
  docker-compose -f docker-compose.yml up -d mysql-server
  docker-compose -f docker-compose.yml up -d postgresql-server
  ```
  
- ***3:mysql数据库初始化*** 

  - 在mysql中创建：summer_cloud、summer_cloud_slave、summer_job、summer_seata、summer_nacos 数据库，作为项目的多数据源配置
  
  - 在 summer_cloud 和 summer_cloud_slave 两个数据库中都执行以下sql文件：<br/>
  [summer base sql](./sql/summer_cloud_project.sql)<br/>
  
  - 在 summer_seata 数据库中都执行以下sql文件：<br/>
  [summer seata sql](./sql/summer_cloud_seata.sql)<br/>

  - 在 summer_job 数据库中都执行以下sql文件：<br/>
  [summer xxljob sql](./sql/summer_cloud_xxljob.sql)<br/>
  [summer snail_job sql](./sql/summer_cloud_snail_job.sql)<br/>
  
  - 在主库 summer_nacos 中执行以下sql文件：<br/>
  [summer nacos sql](./sql/summer_cloud_nacos.sql)
  
- ***4:postgresql数据库初始化*** 
  - 在postgresql中创建：summer_cloud 数据库，作为项目的多数据源配置，并在pgsql中执行一下sql文件：
  [project log sql](./sql/summer_cloud_log(postgresql).sql)

- ***5.初始化nacos***
  - 在本项目根目录执行：
  ```shell
  docker-compose -f docker-compose.yml up -d nacos-server
  ```
  - 打开[nacos web](http://localhost:8848/nacos)端，新增 summer、summer-8、summer-17 三个命名空间，且命名空间ID和命名空间描述都为 summer、summer-8、summer-17
  - 在 summer-8 命名空间中导入[配置文件](./nacos/jdk8/nacos_config_jdk8.zip)
  - 在 summer-17 命名空间中导入[配置文件](./nacos/jdk17/nacos_config_jdk17.zip)

- ***6:启动所有组件***
  - 按次执行以下脚本：
    - 1：[summer_sentinel](./build/sentinel/summer_sentinel.sh)
    - 2：[summer_job](./build/job/summer_job.sh)
    - 3：[summer_elk](./elk/summer_elk_start.sh)
    - 4：[summer_cloud](./summer_start.sh)

#### 5.组件使用信息
|       组件名称       |                     暴露端口                     |       登录用户    |     登录密码      |     使用数据库信息     |
|:----------------:|:--------------------------------------------:|:-------------:|:-------------:|:---------------:|
|      nacos       | [8848](http://localhost:8848/nacos)<br/>9848 |     nacos     |     nacos     |  summer_nacos   |
|      seata       |    [7091](http://localhost:7091)<br/>8091    |     seata     |     seata     |  summer_seata   |
| summer_sentinel  |        [9999](http://localhost:9999)         |   sentinel    |   sentinel    |        /        |
|    summer_job    |      [9093](http://localhost:9093/job)       |     admin     |   Aa@123456   |   summer_job    |
| summer_snail_job |  [18080](http://localhost:18080/snail-job)   |     admin     |     admin     |   summer_job    |
|      minio       |    [9001](http://localhost:9000)<br/>9000    |  minioAdmin   |  minioAdmin   |        /        |
|      mysql       |                     3306                     |     root      | Aabbcc229913! |        /        |
|    postgresql    |                5432<br/>5433                 |   postgres    | Aabbcc229913! |        /        |
|      redis       |                     6379                     |       /       | Aabbcc229913! |      11/12      |
|     rabbitmq     |   5672<br/>[15672](http://localhost:15672)   |     guest     |     guest     |        /        |
|  elasticsearch   |    [9200](http://localhost:9200)<br/>9300    |       /       |       /       |        /        |
|     logstash     |        [4560](http://localhost:4560)         |       /       |       /       |  elasticsearch  |
|      kibana      |        [5601](http://localhost:5601)         |       /       |       /       |  elasticsearch  |
|      zipkin      |        [9411](http://localhost:9411)         |       /       |       /       |  elasticsearch  |
|  summer_auth服务   |    [网关集成swagger](http://localhost:18001)     |  17341787777  | Aa@123456789  |  summer_cloud   |
