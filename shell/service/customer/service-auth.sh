#！/bin/bash

#微服务启动配置shell脚本

#参数说明:              jar激活配置    服务名称    日志文件夹        jar包存放文件夹      集群数，>1则随机端口   指定端口启动，集群数等于1有效
#source serverStart.sh ${activeEnv} ${gateway} ${base_log_path} ${base_file_path} ${gateway_cluster}   9990

source ../env/service-env.sh

bak_file_path="/Users/a-xin/Desktop/jar" #jar包路径
bak_log_path="/Users/a-xin/Desktop/jar/logs" #日志存放路径
bak_env="prod" #springboot激活的环境前缀

file_path=${base_file_path:-${bak_file_path}} #jar包路径
log_path=${base_log_path:-${bak_log_path}} #日志存放路径
env=${active_env:-${bak_env}} #springboot激活的环境前缀

source ../base/checkDir.sh "${log_path}"

summer_auth="summer-auth"
summer_auth_cluster=1
source ../base/serverStart.sh ${env} ${summer_auth} ${log_path} ${file_path} ${summer_auth_cluster} 5011
