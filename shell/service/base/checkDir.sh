#！/bin/bash

#检验是否存在给定的文件夹，不存在则创建文件夹，并赋予777权限

base_log_path=$1

if [ -d "${base_log_path}" ]; then
    echo "===>>> ${base_log_path} 文件夹已存在"
else
    echo "===>>> ${base_log_path} 文件夹不存在,创建文件夹..."
    mkdir -p "${base_log_path}"
fi
chmod -R 777 "${base_log_path}"
