#！/bin/bash

server_name=$1

echo "===>>> 开始结束服务：${server_name} 的进程"
pkill -f "${server_name}"

sleep 2

pid=$(ps -ef | grep "${server_name}" | grep -v grep | awk '{print $2}' | xargs kill -9)
echo "-> ${server_name}的服务进程：${pid}"
for id in $pid
  do
    kill -9 "${id}"
  done
echo "===>>> 已成功结束 ${server_name} 的进程"
