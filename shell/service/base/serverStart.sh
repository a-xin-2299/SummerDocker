#！/bin/bash

#微服务启动脚本，支持单机，集群，指定端口方式启动

#参数说明:              jar激活配置    服务名称    日志文件夹        jar包存放文件夹      集群数，>1则随机端口   指定端口启动，集群数等于1有效
#source serverStart.sh ${activeEnv} ${gateway} ${base_log_path} ${base_file_path} ${gateway_cluster}   9990

# 检查系统是否存在Java环境
if command -v java &> /dev/null; then
    java_version=$(java -version 2>&1 | grep version | awk '{print $3}')
    echo "===>>> 当前Java环境版本: ${java_version}"
else
    echo "===>>> 当前系统未安装Java环境"
    exit 1
fi

active_name=$1
service_name=$2
base_log_path=$3
base_file_path=$4
cluster=$5
port=$6

export LANG="zh_CN.UTF-8"

# 获取所有网卡的IP地址
ip_addresses=$(ifconfig | grep 'inet ' | awk '{print $2}' | cut -d':' -f2)
for ip in $ip_addresses; do
    echo "===>>> 本机 IP 地址为：${ip}"
done

if [ -e "${base_file_path}/${service_name}.jar" ]; then
    echo "===>>> 开始启动 ${service_name} 服务项..."
    sleep 1

    active="-Dspring.profiles.active=${active_name}"

    source serverShutdown.sh "${service_name}"
    if [ "${cluster}" -eq 1 ]; then

      if [ -z "${port}" ]; then

        nohup java -Xms256m -Xmx512m -jar "${active}" "${base_file_path}/${service_name}".jar > "${base_log_path}/${service_name}".log 2>&1 &
        echo "===>>> 查看日志命令: tail -f -n 1000 ${base_log_path}/${service_name}.log"

      else

        active_port="-Dserver.port=${port}"
        nohup java -Xms256m -Xmx512m -jar "${active}" "${active_port}" "${base_file_path}/${service_name}".jar > "${base_log_path}/${service_name}-${port}".log 2>&1 &
        echo "===>>> 查看日志命令: tail -f -n 1000 ${base_log_path}/${service_name}-${port}.log"

      fi
      echo "===>>> ${service_name} 单机服务项已成功启动！！！"

    elif [ "${cluster}" -gt 1 ]; then

      echo "===>>> 开始以集群方式启动服务, 服务名: ${service_name}, 集群数: ${cluster} "
      # 设置端口号范围
      min_port=1024
      max_port=49151

      # 检查端口是否可用的函数
      check_port() {
          local port=$1
          (echo >/dev/tcp/127.0.0.1/"${port}") &>/dev/null
      }

      # shellcheck disable=SC2004
      for ((i = 1; i <= ${cluster}; i++)); do
        # 生成一个随机数
        random_number=$RANDOM
        # 计算在指定范围内的随机端口号
        random_port=$((min_port + random_number % (max_port - min_port + 1)))
        # 验证随机端口是否可用
        while check_port "$random_port"; do
            random_port=$((min_port + RANDOM % (max_port - min_port + 1)))
        done
        server_port="-Dserver.port=${random_port}"
        echo "===>>> 可用的随机端口号: ${random_port}"
        nohup java -Xms256m -Xmx512m -jar "${active}" "${server_port}" "${base_file_path}/${service_name}".jar > "${base_log_path}/${service_name}-${random_port}".log 2>&1 &
        echo "===>>> 查看日志命令: tail -f -n 1000 ${base_log_path}/${service_name}-${random_port}.log"
        sleep 2
      done
      echo "===>>> ${service_name} 集群服务项已成功启动！！！"

    fi
    sleep 2
else
    echo "===>>> ${base_file_path}/${service_name}.jar 文件不存在"
fi
