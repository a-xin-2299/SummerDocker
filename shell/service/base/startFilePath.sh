#！/bin/bash

#配置启动路径类 执行指定文件夹下方所有shell文件
#folder_path="/Users/a-xin/Desktop/testShell"
#source startFilePath.sh ${folder_path}

# 获取脚本本身的绝对路径
script_path=$(readlink -f "$0")
# 指定文件夹路径
folder_path=$1
echo "===>>> 即将执行: ${folder_path} 路径下所有shell脚本..."

# 获取指定文件夹下所有以.sh结尾的文件
sh_files=("${folder_path}"/*.sh)

# 遍历文件并执行
for file in "${sh_files[@]}"; do
    if [ -f "${file}" ]; then
        # 获取文件的绝对路径
        absolute_path=$(readlink -f "${file}")
        # 比较绝对路径是否相同
        if [ "${script_path}" == "${absolute_path}" ]; then
            echo "===>>> 需要执行的文件与本执行文件相同，跳过执行..."
        else
            echo "===>>> The shell file is will to be executed: ${file}"
            chmod 777 "${file}"
            sh "${file}"
            sleep 2
        fi
    fi
done
