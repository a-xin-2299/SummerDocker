#！/bin/bash

#nacos配置信息
host=127.0.0.1
port=8848
group=SEATA_GROUP
namespace=a-xin
user=nacos
password=nacos
#注意，需要将config.txt放在此目录文件下，同时修改config.txt中数据库连接信息！！！
# shellcheck disable=SC2039
source nacosConfig.sh ${host} ${port} ${group} ${namespace} ${user} ${password}