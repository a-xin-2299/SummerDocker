#！/bin/bash

host=$1
port=$2
group=$3
tenant=$4
username=$5
password=$6

if [ -z ${host} ]; then
    host=localhost
fi
if [ -z ${port} ]; then
    port=8848
fi
if [ -z ${group} ]; then
    group="SEATA_GROUP"
fi
if [ -z ${tenant} ]; then
    tenant=""
fi
if [ -z ${username} ]; then
    username=""
fi
if [ -z ${password} ]; then
    password=""
fi

# 检查系统是否存在Java环境
# shellcheck disable=SC2039
if command -v java &> /dev/null; then
    java_version=$(java -version 2>&1 | grep version | awk '{print $3}')
    echo "===>>> 当前Java环境版本: ${java_version}"
else
    echo "===>>> 当前系统未安装Java环境"
    exit 1
fi

nacosAddr=${host}:${port}
contentType="content-type:application/json;charset=UTF-8"

echo "===>>> set nacosAddr=${nacosAddr}"
echo "===>>> set group=${group}"
echo "===>>> set tenant=${tenant}"
echo "===>>> set username=${username}"
echo "===>>> set password=${password}"

urlencode() {
  length="${#1}"
  i=0
  while [ "$length" -gt $i ]; do
    # shellcheck disable=SC2039
    char="${1:$i:1}"
    case $char in
    [a-zA-Z0-9.~_-]) printf $char ;;
    *) printf '%%%02X' "'$char" ;;
    esac
    i=$(expr $i + 1)
  done
}

failCount=0
tempLog=$(mktemp -u)
# shellcheck disable=SC2112
function addConfig() {
  dataId=$(urlencode $1)
  content=$(urlencode $2)
  curl -X POST -H "${contentType}" "http://$nacosAddr/nacos/v1/cs/configs?dataId=$dataId&group=$group&content=$content&tenant=$tenant&username=$username&password=$password" >"${tempLog}" 2>/dev/null
  # shellcheck disable=SC2046
  if [ -z $(cat "${tempLog}") ]; then
    echo "===>>> Please check the cluster status. "
    exit 1
  fi
  # shellcheck disable=SC2039
  if [ "$(cat "${tempLog}")" == "true" ]; then
    echo "===>>> Set $1=$2 successfully "
  else
    echo "===>>> Set $1=$2 failure "
    # shellcheck disable=SC2003
    failCount=$(expr $failCount + 1)
  fi
}

count=0
COMMENT_START="#"
# shellcheck disable=SC2013
for line in $(cat $(dirname "$0")/config.txt | sed s/[[:space:]]//g); do
    # shellcheck disable=SC2039
    if [[ "$line" =~ ^"${COMMENT_START}".*  ]]; then
      continue
    fi
    count=`expr $count + 1`
	  key=${line%%=*}
    value=${line#*=}
	  addConfig "${key}" "${value}"
done

echo "========================================================================="
echo " ===>>> Complete initialization parameters,  total-count:$count ,  failure-count:$failCount "
echo "========================================================================="

if [ "${failCount}" -eq 0 ]; then
	echo "===>>> Init nacos config finished, please start seata-server. "
else
	echo "===>>> init nacos config fail. "
fi
