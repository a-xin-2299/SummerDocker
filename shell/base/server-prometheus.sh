#！/bin/bash

prometheus=$1
base_file_path=$2
base_log_path=$3

prometheus_log_name="${prometheus}.log"
bash checkFile.sh "${base_log_path}${prometheus_log_name}"

prometheus_path="${base_file_path}${prometheus}"
echo "prometheus启动路径===>>> ${prometheus_path}"

cd "${prometheus_path}" || exit
nohup ./prometheus --config.file=prometheus.yml > "${base_log_path}${prometheus_log_name}" 2>&1 &

echo "===>>> prometheus服务已启动:"
echo "===>>> 服务地址为：localhost:9090/targets?search="
echo "=========================================="
