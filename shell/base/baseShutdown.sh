#！/bin/bash

echo "<<<===开始停止整机服务项===>>> "

redis="redis-7.0.5"
nacos="nacos-2.3.0"
seata="seata-1.5.2"
minio="minio"
sentinel="sentinel-1.8.0"
xxl_job="XXLJob-1.0.0"
prometheus="prometheus"
grafana="grafana"
node_exporter="node_exporter"
zipkin="zipkin"
nginx="nginx-1.25.3"

cd "/Users/a-xin/Downloads/software/${nginx}/sbin" || exit
echo "229913" | sudo -S ./nginx -s stop -c /Users/a-xin/Downloads/software/${nginx}/conf/nginx.conf
echo "===>>> kill nginx <--> 进程成功!"

rabbitmqctl stop
echo "===>>> kill RabbitMQ <--> 进程成功!"

sleep 1
pkill -f ${prometheus}
echo "===>>> kill ${prometheus} <--> 进程成功!"

sleep 1
pkill -f ${grafana}
echo "===>>> kill ${grafana} <--> 进程成功!"

sleep 1
pkill -f ${node_exporter}
echo "===>>> kill ${node_exporter} <--> 进程成功!"

sleep 1
pkill -f ${zipkin}
echo "===>>> kill ${zipkin} <--> 进程成功!"

sleep 1
pkill -f ${seata}
echo "===>>> kill ${seata} <--> 进程成功!"

sleep 1
pkill -f ${sentinel}
echo "===>>> kill ${sentinel} <--> 进程成功!"

sleep 1
pkill -f ${redis}
echo "===>>> kill ${redis} <--> 进程成功!"

sleep 1
pkill -f ${nacos}
echo "===>>> kill ${nacos} <--> 进程成功!"

sleep 1
pkill -f ${xxl_job}
echo "===>>> kill ${xxl_job} <--> 进程成功!"

sleep 1
pkill -f ${minio}
echo "===>>> kill ${minio} <--> 进程成功!"
