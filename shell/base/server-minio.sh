#！/bin/bash

minio=$1
base_file_path=$2
base_log_path=$3

minio_data="minio_data"
minio_log_name="${minio}.log"
minio_data_base_path="${base_file_path}${minio_data}"

bash checkDir.sh "${minio_data_base_path}"
bash checkFile.sh "${base_log_path}${minio_log_name}"

echo "minio启动路径===>>> ${base_file_path}"

cd "${minio_data_base_path}" || exit
nohup minio server "${minio_data_base_path}" > "${base_log_path}${minio_log_name}" 2>&1 &
#nohup ./minio-linux server "${minio_data_base_path}" --console-address ":9000" > "${base_log_path}${minio_log_name}" 2>&1 &

echo "===>>> minio服务已启动:"
echo "===>>> 服务地址为：localhost:9000"
echo "===>>> 服务面板地址为：localhost:9000/login"
echo "===>>> minio数据文件：${minio_data_base_path}"
echo "===>>> minio日志文件：${base_log_path}${minio_log_name}"
echo "===>>> 登录用户名：minioadmin， 登录密码：minioadmin"
echo "=========================================="
