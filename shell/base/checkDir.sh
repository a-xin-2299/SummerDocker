#！/bin/bash

check_path=$1

#如果文件夹不存在，则创建文件夹，并赋权777
if [ ! -d "${check_path}" ]; then
  mkdir -P "${check_path}"
  chmod -R 777 "${check_path}"
fi
