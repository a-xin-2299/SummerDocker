#！/bin/bash

node_exporter=$1
base_file_path=$2
base_log_path=$3

node_exporter_log_name="${node_exporter}.log"
sh checkFile.sh "${base_log_path}${node_exporter_log_name}"

node_exporter_path="${base_file_path}${node_exporter}"
echo "node_exporter启动路径===>>> ${node_exporter_path}"

cd "${node_exporter_path}" || exit
nohup ./node_exporter > "${base_log_path}${node_exporter_log_name}" 2>&1 &

echo "===>>> node_exporter服务已启动:"
echo "===>>> 服务地址为：localhost:9100/metrics"
echo "=========================================="
