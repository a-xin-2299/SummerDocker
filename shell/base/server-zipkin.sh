#！/bin/bash

zipkin=$1
base_file_path=$2
base_log_path=$3

zipkin_log_name="${zipkin}.log"
bash checkFile.sh "${base_log_path}${zipkin_log_name}"

echo "zipkin启动路径===>>> ${base_file_path}"
cd "${base_file_path}" || exit
nohup java -jar zipkin.jar --STORAGE_TYPE=mysql --MYSQL_HOST=127.0.0.1 --MYSQL_TCP_PORT=3306 --MYSQL_USER=root --MYSQL_PASS=Aabbcc229913! --MYSQL_DB=project > "${base_log_path}${zipkin_log_name}" 2>&1 &
echo "===>>> zipkin服务已启动:"
echo "===>>> 服务地址为：localhost:9411/zipkin"
echo "=========================================="
