#！/bin/bash

xxl_job=$1
base_file_path=$2
base_log_path=$3
jvm_param=$4

xxl_job_log_name="${xxl_job}.log"
bash checkFile.sh "${base_log_path}${xxl_job_log_name}"

echo "XXLJob启动路径===>>> ${base_file_path}"

cd "${base_file_path}" || exit
nohup java -Dspring.cloud.nacos.discovery.namespace=project ${jvm_param} -jar  "${xxl_job}.jar" > "${base_log_path}${xxl_job_log_name}" 2>&1 &

echo "===>>> XXLJob服务已启动:"
echo "===>>> 服务地址为：localhost:9093/job/toLogin"
echo "===>>> 登录用户名：admin， 登录密码：Aa@123456"
echo "=========================================="
