#！/bin/bash

# 检查系统是否存在Java环境
if command -v java &> /dev/null; then
    java_version=$(java -version 2>&1 | grep version | awk '{print $3}')
    echo "===>>> 当前Java环境版本: ${java_version}"
else
    echo "===>>> 当前系统未安装Java环境"
    exit 1
fi
