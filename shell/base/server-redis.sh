#！/bin/bash

redis=$1
base_file_path=$2
base_log_path=$3

redis_log_name="redis-7.0.5.log"
bash checkFile.sh "${base_log_path}${redis_log_name}"

redis_path="${base_file_path}${redis}"
echo "redis启动路径===>>> ${redis_path}"

cd "${redis_path}" || exit
nohup redis-server "${redis_path}/redis.conf" --logfile "${base_log_path}${redis_log_name}" > "${base_log_path}${redis_log_name}" 2>&1 &

echo "===>>> redis服务已启动:"
echo "===>>> 服务地址为：localhost:6379 "
echo "===>>> 服务密码为：Aabbcc229913!"
echo "=========================================="
