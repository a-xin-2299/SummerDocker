#！/bin/bash

nacos=$1
base_file_path=$2
base_log_path=$3

nacos_log_name="${nacos}.log"
bash checkFile.sh "${base_log_path}${nacos_log_name}"

nacos_path="${base_file_path}${nacos}"
echo "nacos启动路径===>>> ${nacos_path}"

cd "${nacos_path}/bin" || exit
chmod 777 startup.sh
nohup ./startup.sh -m standalone > "${base_log_path}${nacos_log_name}" 2>&1 &

echo "===>>> nacos服务已启动:"
echo "===>>> 服务地址为：localhost:8848/nacos "
echo "===>>> 登录用户名：nacos， 登录密码：nacos"
echo "=========================================="
