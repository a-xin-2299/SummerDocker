#！/bin/bash

echo "<<<=====开始启动整机服务项=====>>>"

# shellcheck disable=SC1090
bash ~/.bash_profile
brew cleanup

bash checkJdkVersion.sh

# 获取所有网卡的IP地址
ip_addresses=$(ifconfig | grep 'inet ' | awk '{print $2}' | cut -d':' -f2)
for ip in ${ip_addresses}; do
    echo "===>>> 本机 IP 地址为：${ip}"
done

base_file_path="/Users/a-xin/Downloads/software/"
base_log_path="${base_file_path}logs/"

bash checkDir.sh ${base_file_path}
bash checkDir.sh ${base_log_path}

jvm_param="-Xms1024M -Xmx1024M -Xmn512M -Xss512K -XX:MetaspaceSize=256M"

echo "开始启动基础服务项===>>>"

#bash server-rabbitmq.sh

sleep 2
nacos="nacos-2.3.0"
bash server-nacos.sh ${nacos} ${base_file_path} ${base_log_path}

#sleep 2
#nginx="nginx-1.25.3"
#bash server-nginx.sh ${nginx} ${base_file_path} ${base_log_path}

#sleep 2
#zipkin="zipkin"
#bash server-zipkin.sh ${zipkin} ${base_file_path} ${base_log_path}

#sleep 2
#mongodb="mongodb-6.0.15"
#bash server-mongodb.sh ${mongodb} ${base_file_path} ${base_log_path}

#sleep 2
#node_exporter="node_exporter"
#bash server-node-exporter.sh ${node_exporter} ${base_file_path} ${base_log_path}
#
#sleep 2
#prometheus="prometheus"
#bash server-prometheus.sh ${prometheus} ${base_file_path} ${base_log_path}
#
#sleep 2
#grafana="grafana"
#bash server-grafana.sh ${grafana} ${base_file_path} ${base_log_path}

#sleep 2
#redis="redis-7.0.5"
#bash server-redis.sh ${redis} ${base_file_path} ${base_log_path}
#
#sleep 2
#minio="minio"
#bash server-minio.sh ${minio} ${base_file_path} ${base_log_path}

sleep 2
bash server-nacos.sh ${nacos} ${base_file_path} ${base_log_path}

#--------以下基础服务需要依赖于nacos，nacos必须早于以下服务执行，启动脚本会延迟5s执行-------#
#seata="seata-1.5.2"
#port="8091"
#host="192.168.70.56"
#bash server-seata.sh ${seata} ${base_file_path} ${base_log_path} ${host} ${port}

#seata1="seata-2.0"
#port1="18091"
#host1="192.168.70.56"
#bash server-seata.sh ${seata1} ${base_file_path} ${base_log_path} ${host1} ${port1}

#xxl_job="XXLJob-1.0.0"
#bash server-summer_job.sh ${xxl_job} ${base_file_path} ${base_log_path} ${jvm_param}
#
#sentinel="sentinel-1.8.0"
#bash server-summer_sentinel.sh ${sentinel} ${base_file_path} ${base_log_path} ${jvm_param}

echo "
==============================================================================
                <<<=====整机基础服务启动完毕！！！=====>>>"
