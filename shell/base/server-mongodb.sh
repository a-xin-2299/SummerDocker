#！/bin/bash

mongodb=$1
base_file_path=$2
base_log_path=$3

mongodb_log_name="${mongodb}.log"

bash checkFile.sh "${base_log_path}${mongodb_log_name}"

echo "mongodb启动路径===>>> ${base_file_path}"

cd "${base_file_path}${mongodb}/bin" || exit
nohup mongod -f /Users/a-xin/Downloads/software/mongodb-6.0.15/conf/mongodb.conf > "${base_log_path}${mongodb_log_name}" 2>&1 &

echo "===>>> mongodb服务已启动:"
echo "===>>> 服务地址为：localhost:27017"
echo "===>>> minio日志文件：${base_log_path}${mongodb_log_name}"
echo "===>>> 登录用户名：root， 登录密码：Aabbcc229913!"
echo "=========================================="
