#！/bin/bash

seata=$1
base_file_path=$2
base_log_path=$3
host=$4
port=$5

seata_log_name="${seata}.log"
bash checkFile.sh "${base_log_path}${seata_log_name}"

seata_path="${base_file_path}${seata}"
echo "seata启动路径===>>> ${seata_path}"

cd "${seata_path}/bin" || exit
chmod 777 seata-server.sh
nohup ./seata-server.sh -h "${host}" -p "${port}" > "${base_log_path}${seata_log_name}" 2>&1 &

echo "===>>> seata服务已启动:"
echo "===>>> 服务地址为：localhost:${port}"
port=$[$port - 1000]
echo "===>>> 服务面板地址为：localhost:${port}/#/login"
echo "===>>> 登录用户名：seata， 登录密码：seata"
echo "=========================================="
