#！/bin/bash

sentinel=$1
base_file_path=$2
base_log_path=$3
jvm_param=$4

sentinel_log_name="${sentinel}.log"
bash checkFile.sh "${base_log_path}${sentinel_log_name}"

cd "${base_file_path}" || exit
nohup java ${jvm_param} -Dcsp.sentinel.dashboard.server=localhost:9999 -Dspring.cloud.nacos.discovery.namespace=project -Dproject.name="${sentinel}" -jar "${sentinel}.jar" > "${base_log_path}${sentinel_log_name}" 2>&1 &
echo "===>>> sentinel服务已启动:"
echo "===>>> 服务地址为：localhost:9999/#/login"
echo "===>>> sentinel日志文件：${base_log_path}${sentinel_log_name}"
echo "===>>> 登录用户名：sentinel， 登录密码：sentinel"
