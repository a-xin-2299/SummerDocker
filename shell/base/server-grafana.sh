#！/bin/bash

grafana=$1
base_file_path=$2
base_log_path=$3

grafana_log_name="${grafana}.log"
bash checkFile.sh "${base_log_path}${grafana_log_name}"

grafana_path="${base_file_path}${grafana}"
echo "grafana启动路径===>>> ${grafana_path}"

cd "${grafana_path}/bin" || exit
nohup ./grafana > "${base_log_path}${grafana_log_name}" 2>&1 &

echo "===>>> grafana服务已启动:"
echo "===>>> 服务地址为：localhost:3000/login"
echo "===>>> 登录用户名：admin， 登录密码：admin"
echo "=========================================="
