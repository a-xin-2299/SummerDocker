#！/bin/bash

nginx=$1
base_file_path=$2
base_log_path=$3

nginx_log_name="${nginx}.log"
bash checkFile.sh "${base_log_path}${nginx_log_name}"

echo "nginx启动路径===>>> ${base_file_path}${nginx}"
cd "${base_file_path}${nginx}/sbin" || exit

echo "229913" | sudo -S nohup ./nginx -c "${base_file_path}${nginx}"/conf/nginx.conf > "${base_log_path}${nginx_log_name}" 2>&1 &

echo "===>>> nginx服务已启动:"
echo "===>>> 服务地址为：localhost:80"
echo "=========================================="
