#！/bin/bash

file_path=$1

if [ ! -f "${file_path}" ]; then
  touch "${file_path}"
  chmod -R 777 "${file_path}"
fi
