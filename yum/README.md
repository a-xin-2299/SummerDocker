***1：卸载python和yum旧的安装包***

- 执行以下两个shell脚本文件：<br/>
  [uninstall_python](/yum/uninstall/uninstall_python.sh)<br/>
  [uninstall_yum](/yum/uninstall/uninstall_yum.sh)<br/>

***2：查看当前服务器版本信息，我这里是：7.9.2009***

```shell
cat /etc/redhat-release
```

***3：下载rpm包***

点击进入[阿里镜像站](https://mirrors.aliyun.com/centos/)下载安装需求包文件，一共14个，可以直接搜索下载

```
python-2.7.5-89.el7.x86_64.rpm
python-devel-2.7.5-89.el7.x86_64.rpm
python-iniparse-0.4-9.el7.noarch.rpm
python-libs-2.7.5-89.el7.x86_64.rpm
python-pycurl-7.19.0-19.el7.x86_64.rpm
python-setuptools-0.9.8-7.el7.noarch.rpm
python-urlgrabber-3.10-10.el7.noarch.rpm
rpm-python-4.11.3-45.el7.x86_64.rpm
yum-3.4.3-168.el7.centos.noarch.rpm
yum-metadata-parser-1.1.4-10.el7.x86_64.rpm
yum-plugin-aliases-1.1.31-54.el7_8.noarch.rpm
yum-plugin-fastestmirror-1.1.31-54.el7_8.noarch.rpm
yum-plugin-protectbase-1.1.31-54.el7_8.noarch.rpm
yum-utils-1.1.31-54.el7_8.noarch.rpm
```

在[层级目录](./rpm)下方已存放下载好的文件

***4：安装rpm包***

将[层级目录](./rpm)下方文件上传至服务器指定目录，在此目录下执行：

```shell
rpm -ivh *.rpm --nodeps --force
```

***5：更换yum源地址***

- -> 由于CentOS7默认的yum源是国外的，导致我们使用yum下载软件的下载速度不是很理想，这时候我们就需要将yum源更换成国内的源
- -> 首先我们先对系统本身的yum源进行备份
  ```shell
  mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
  ```
- -> 手动下载国内yum源配置文件，已在[层级目录](./repo)下方存放阿里云下载的yum配置文件

  [阿里云 CentOS 7 源（推荐）](http://mirrors.aliyun.com/repo/Centos-7.repo)</br>
  [网易163 CentOS 7 源](http://mirrors.163.com/.help/CentOS7-Base-163.repo)</br>
  [搜狐 CentOS 7 源](http://mirrors.sohu.com/help/CentOS7-Base-sohu.repo)</br>
  [华为云 CentOS 7 源](http://mirrors.myhuaweicloud.com/repo/CentOS-7.repo)</br>
  [华东理工大学 CentOS 7 源](https://mirrors.ustc.edu.cn/centos/7/os/x86_64)</br>
  [清华大学 CentOS 7 源](https://mirrors.tuna.tsinghua.edu.cn/centos/7/os/x86_64/)</br>
  [北京理工大学 CentOS 7 源](https://mirrors.bfsu.edu.cn/centos/7/os/x86_64/)</br>
  [上海交通大学 CentOS 7 源](https://ftp.sjtu.edu.cn/centos/7/os/x86_64/)</br>
  [中国科学技术大学 CentOS 7 源](https://mirrors.ustc.edu.cn/centos/7/os/x86_64/)</br>
  [兰州大学 CentOS 7 源](https://mirror.lzu.edu.cn/centos/7/os/x86_64/)</br>

- -> 下载完改完名之后，把CentOS-Base.repo 放到 /etc/yum.repos.d/下

***6：更新yum缓存***

```shell
yum clean all
yum makecache
yum update
```

***7：使用yum安装wegt***

```shell
sudo yum install wget
```
