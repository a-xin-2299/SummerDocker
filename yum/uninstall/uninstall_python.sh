#！/bin/bash

#强制删除现有的python安装程序和组件
rpm -qa|grep python|xargs rpm -ev --allmatches --nodeps
#删除所有python的残余文件
whereis python |xargs rm -frv
#验证是否卸载成功，成功则无返回值
whereis python
