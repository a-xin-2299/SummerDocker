#！/bin/bash

#强制删除现有的yum安装程序和组件
rpm -qa|grep yum|xargs rpm -ev --allmatches --nodeps
#删除所有yum的残余文件
whereis yum |xargs rm –frv
#卸载完成后根据系统的版本号找到相应的python和yum的包，系统版本号查询
cat /etc/redhat-release
