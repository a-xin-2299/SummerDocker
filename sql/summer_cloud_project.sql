/*
 Navicat Premium Dump SQL

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80037 (8.0.37)
 Source Host           : localhost:3306
 Source Schema         : summer_cloud

 Target Server Type    : MySQL
 Target Server Version : 80037 (8.0.37)
 File Encoding         : 65001

 Date: 02/01/2025 13:35:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for product_info
-- ----------------------------
DROP TABLE IF EXISTS `product_info`;
CREATE TABLE `product_info` (
                              `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                              `create_time` datetime(6) NOT NULL COMMENT '创建时间',
                              `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                              `del` int NOT NULL COMMENT '删除标识：1正常，0删除',
                              `update_time` datetime(6) NOT NULL COMMENT '更新时间',
                              `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                              `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID，租户ID',
                              `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户手机号',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='product-产品信息';

-- ----------------------------
-- Records of product_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for product_order_info
-- ----------------------------
DROP TABLE IF EXISTS `product_order_info`;
CREATE TABLE `product_order_info` (
                                    `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                                    `create_time` datetime(6) NOT NULL COMMENT '创建时间',
                                    `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                                    `del` int NOT NULL COMMENT '删除标识：1正常，0删除',
                                    `update_time` datetime(6) NOT NULL COMMENT '更新时间',
                                    `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                                    `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID，租户ID',
                                    `order_state` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单状态',
                                    `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单备注信息',
                                    `pay_time` datetime DEFAULT NULL COMMENT '订单支付时间',
                                    `all_price` decimal(10,2) DEFAULT NULL COMMENT '订单总价',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='product-订单信息';

-- ----------------------------
-- Records of product_order_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sms_template
-- ----------------------------
DROP TABLE IF EXISTS `sms_template`;
CREATE TABLE `sms_template` (
                              `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                              `template_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信模板编号',
                              `temp_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板描述',
                              `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '使用类型',
                              `server` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '隶属平台',
                              `del` tinyint NOT NULL COMMENT '是否删除',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='短信下发日志记录';

-- ----------------------------
-- Records of sms_template
-- ----------------------------
BEGIN;
INSERT INTO `sms_template` (`id`, `template_id`, `temp_desc`, `type`, `server`, `del`) VALUES ('1736679586025996290', '0012321', '测试短信模版', 'LOG_IN', 'tencent', 1);
INSERT INTO `sms_template` (`id`, `template_id`, `temp_desc`, `type`, `server`, `del`) VALUES ('1736679801177014274', '0012321', '测试短信模版ali', 'LOG_IN', 'ali', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_api
-- ----------------------------
DROP TABLE IF EXISTS `sys_api`;
CREATE TABLE `sys_api` (
                         `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                         `api` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'api全路径',
                         `api_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'api描述',
                         `server` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'api归属服务',
                         `create_time` datetime(6) NOT NULL COMMENT '创建时间',
                         `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                         `del` int NOT NULL COMMENT '删除标识：1正常，0删除',
                         `update_time` datetime(6) NOT NULL COMMENT '更新时间',
                         `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                         `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求方式',
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-部门信息表，父子结构';

-- ----------------------------
-- Records of sys_api
-- ----------------------------
BEGIN;
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302863917058', '/sys/route/routeBindApi', '路由绑定api信息', 'summer-sys', '2024-08-23 16:32:29.034000', '1', 1, '2024-12-19 10:30:36.702000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471490', '/sys/route/deleteRoute', '删除路由信息', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.708000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471491', '/sys/role/getAllRole', '获取所有角色信息', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.714000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471492', '/sys/project/createTable', '根据表格生成对应的代码', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-10-06 20:26:26.473000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471493', '/sys/user/deleteUser', '删除用户', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.717000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471494', '/sys/user/changePassword', '修改密码', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.721000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471495', '/sys/sms/deleteTemplate', '删除短信模板', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.729000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471496', '/sys/warm-up', '/sys/warm-up', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.733000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302897471497', '/sys/userFeign/getUserInfoByPhone', '/sys/userFeign/getUserInfoByPhone', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.736000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665794', '/sys/route/addRoute', '添加路由信息', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.741000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665795', '/sys/role/roleBindRoute', '角色绑定路由信息', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.745000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665796', '/sys/project/createServer', '生成单个微服务架构', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-10-06 20:26:26.500000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665797', '/sys/user/userBindRole', '用户绑定角色', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.747000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665798', '/sys/dataSource/getTablesByUrl', '获取指定数据库表信息', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-10-06 20:26:26.506000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665799', '/sys/route/updateRoute', '更新路由信息', 'summer-sys', '2024-08-23 16:32:29.035000', '1', 1, '2024-12-19 10:30:36.750000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665800', '/sys/userFeign/getUserAuth', '/sys/userFeign/getUserAuth', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.753000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665801', '/sys/sys/getWebFile', '根据网络文件地址下载到本地', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.757000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665802', '/sys/dataSource/delRedisData', '操作redis链接', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-10-06 20:26:26.518000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665803', '/sys/sms/addTemplate', '添加短信模版', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.760000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665804', '/sys/role/delRole', '删除角色信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.765000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665805', '/sys/sms/getTemplate', '获取短信模版信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.775000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665806', '/sys/user/getUserRoute', '获取登录用户路由信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.779000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665807', '/sys/sys/createQRCode', '根据内容生成二维码', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.782000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665808', '/sys/user/getUserInfo', '获取登录用户信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.785000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665809', '/sys/role/addRole', '新增角色信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.788000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665810', '/sys/user/registrationApp', '用户注册APP', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.794000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302901665811', '/sys/role/updateRole', '更新角色信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.798000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860098', '/sys/user/getAllUsers', '获取所有用户信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.801000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860099', '/sys/tenant/addTenant', '新增区域信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.803000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860100', '/sys/sys/testApi', '测试API接口', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.806000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860101', '/sys/route/getRoute', '获取路由信息', 'summer-sys', '2024-08-23 16:32:29.036000', '1', 1, '2024-12-19 10:30:36.809000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860102', '/sys/user/registrationWeb', '用户注册WEB', 'summer-sys', '2024-08-23 16:32:29.037000', '1', 1, '2024-12-19 10:30:36.812000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860103', '/sys/user/forgetPassword', '忘记密码', 'summer-sys', '2024-08-23 16:32:29.037000', '1', 1, '2024-12-19 10:30:36.816000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860104', '/sys/user/updateUser', '更新用户信息', 'summer-sys', '2024-08-23 16:32:29.037000', '1', 1, '2024-12-19 10:30:36.824000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860105', '/sys/user/userUnBindRole', '用户取消绑定角色', 'summer-sys', '2024-08-23 16:32:29.037000', '1', 1, '2024-12-19 10:30:36.832000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1826900302905860106', '/sys/dataSource/getTableDetail', '获取数据库表字段信息', 'summer-sys', '2024-08-23 16:32:29.037000', '1', 1, '2024-10-06 20:26:26.578000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677493977903105', '/calculate/task/deleteTaskInfo', '删除定时任务', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.014000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400577', '/calculate/log/getSmsLog', '分页获取短信下发日志', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.026000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400578', '/calculate/task/getTaskInfo', '分页获取定时任务', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.034000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400579', '/calculate/log/getSysLog', '分页获取API访问日志', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.038000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400580', '/calculate/task/operationTask', '操作定时任务', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.043000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400581', '/calculate/log/getGatewayLog', '分页获取网关访问日志', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.047000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400582', '/calculate/task/updateTaskInfo', '更新定时任务信息', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.054000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400583', '/calculate/warm-up', '/calculate/warm-up', 'summer-calculate', '2024-09-19 16:03:33.057000', '1', 1, '2024-09-19 16:07:18.057000', '1', 'GET');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400584', '/calculate/task/addTaskInfo', '新增定时任务', 'summer-calculate', '2024-09-19 16:03:33.058000', '1', 1, '2024-09-19 16:07:18.059000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400585', '/calculate/task/updateTask', '更新定时任务', 'summer-calculate', '2024-09-19 16:03:33.058000', '1', 1, '2024-09-19 16:07:18.062000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400586', '/calculate/task/getDefinedTaskInfo', '分页获取已定义的定时任务', 'summer-calculate', '2024-09-19 16:03:33.058000', '1', 1, '2024-09-19 16:07:18.065000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1836677494053400587', '/calculate/log/getSysTaskLog', '分页获取定时任务执行日志', 'summer-calculate', '2024-09-19 16:03:33.058000', '1', 1, '2024-09-19 16:07:18.067000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1842553769085743105', '/sys/login', '用户名密码登录', 'summer-sys', '2024-10-05 21:13:46.201000', '1', 1, '2024-12-19 10:30:36.838000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1842559626318000130', '/sys/smsLogin', '手机验证码登录', 'summer-sys', '2024-10-05 21:37:02.674000', '1', 1, '2024-12-19 10:30:36.848000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1842559626326388738', '/sys/logout', '登出处理', 'summer-sys', '2024-10-05 21:37:02.675000', '1', 1, '2024-12-19 10:30:36.861000', '1', 'POST');
INSERT INTO `sys_api` (`id`, `api`, `api_desc`, `server`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `method`) VALUES ('1842559626326388739', '/sys/appLogin', '手机小程序登录', 'summer-sys', '2024-10-05 21:37:02.675000', '1', 1, '2024-12-19 10:30:36.871000', '1', 'POST');
COMMIT;

-- ----------------------------
-- Table structure for sys_api_route
-- ----------------------------
DROP TABLE IF EXISTS `sys_api_route`;
CREATE TABLE `sys_api_route` (
                               `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                               `route_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '路由id',
                               `api_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'api id',
                               `del` int NOT NULL COMMENT '数据删除标识：1正常，0删除',
                               `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                               `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                               `update_time` datetime NOT NULL COMMENT '更新时间',
                               `create_time` datetime NOT NULL COMMENT '创建时间',
                               `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID。租户ID',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-路由权限关联';

-- ----------------------------
-- Records of sys_api_route
-- ----------------------------
BEGIN;
INSERT INTO `sys_api_route` (`id`, `route_id`, `api_id`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`) VALUES ('1', '1742805722777264130', '1797804665895718913', 1, '1', '1', '2024-05-24 11:33:49', '2024-05-24 11:33:51', '1');
INSERT INTO `sys_api_route` (`id`, `route_id`, `api_id`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`) VALUES ('2', '1742806434684911618', '1797804665891524621', 1, '1', '1', '2024-05-24 11:33:49', '2024-05-24 11:33:51', '1');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
                          `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                          `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
                          `del` int NOT NULL COMMENT '数据删除标识：1正常，0删除',
                          `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                          `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                          `update_time` datetime NOT NULL COMMENT '更新时间',
                          `create_time` datetime NOT NULL COMMENT '创建时间',
                          `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID。租户ID',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`id`, `name`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`) VALUES ('1823955804764516354', '1231111', 1, '1731567974296924161', '1731567974296924161', '2024-08-15 13:32:06', '2024-08-15 13:32:06', '1731567974296924163');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
                               `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                               `role_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
                               `user_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户ID',
                               `del` int NOT NULL COMMENT '数据删除标识：1正常，0删除',
                               `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                               `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                               `update_time` datetime NOT NULL COMMENT '更新时间',
                               `create_time` datetime NOT NULL COMMENT '创建时间',
                               `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID。租户ID',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-用户角色信息表';

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_route
-- ----------------------------
DROP TABLE IF EXISTS `sys_route`;
CREATE TABLE `sys_route` (
                           `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                           `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
                           `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单编码',
                           `route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单路由地址',
                           `open` int NOT NULL COMMENT '是否开启： 1正常，0关闭',
                           `del` int NOT NULL COMMENT '数据删除标识：1正常，0删除',
                           `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                           `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                           `update_time` datetime NOT NULL COMMENT '更新时间',
                           `create_time` datetime NOT NULL COMMENT '创建时间',
                           `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID。租户ID',
                           `priority` int NOT NULL COMMENT '优先级别',
                           `p_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父级ID',
                           `lft` int DEFAULT NULL COMMENT '左值',
                           `rgt` int DEFAULT NULL COMMENT '右值',
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-路由信息表';

-- ----------------------------
-- Records of sys_route
-- ----------------------------
BEGIN;
INSERT INTO `sys_route` (`id`, `name`, `code`, `route`, `open`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`, `priority`, `p_id`, `lft`, `rgt`) VALUES ('1742805722777264130', '测试路路由', 'TEST-001', '/test/test001', 1, 1, '1731567974296924161', '1731567974296924161', '2024-01-04 15:10:39', '2024-01-04 15:10:39', '1731567974296924163', 0, '-1', 1, 6);
INSERT INTO `sys_route` (`id`, `name`, `code`, `route`, `open`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`, `priority`, `p_id`, `lft`, `rgt`) VALUES ('1742806434684911618', '测试路路由002', 'TEST-002', '/test/test002', 1, 1, '1731567974296924161', '1731567974296924161', '2024-01-04 15:13:28', '2024-01-04 15:13:28', '1731567974296924163', 0, '-1', 2, 5);
INSERT INTO `sys_route` (`id`, `name`, `code`, `route`, `open`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`, `priority`, `p_id`, `lft`, `rgt`) VALUES ('1742806573939998721', '测试路路由003', 'TEST-003', '/test/test003', 1, 1, '1731567974296924161', '1731567974296924161', '2024-01-04 15:14:02', '2024-01-04 15:14:02', '1731567974296924163', 99, '1742806434684911618', 3, 4);
INSERT INTO `sys_route` (`id`, `name`, `code`, `route`, `open`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`, `priority`, `p_id`, `lft`, `rgt`) VALUES ('1752921832629567489', '测试路路由004', 'CODE', '/test/test004', 0, 1, '1746808631422201857', '1746808631422201857', '2024-02-01 13:08:27', '2024-02-01 13:08:27', '1731567974296924163', 0, '1742806434684911618', 1, 6);
INSERT INTO `sys_route` (`id`, `name`, `code`, `route`, `open`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`, `priority`, `p_id`, `lft`, `rgt`) VALUES ('1754311549916864514', '测试路路由005', 'CODE-1', '/test/test005', 0, 1, '1746808631422201857', '1746808631422201857', '2024-02-05 09:10:42', '2024-02-05 09:10:42', '1731567974296924163', 0, '1742806434684911618', 2, 5);
INSERT INTO `sys_route` (`id`, `name`, `code`, `route`, `open`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`, `priority`, `p_id`, `lft`, `rgt`) VALUES ('1754315142447251457', '测试路路由006', 'CODE-2', '/test/test006', 0, 1, '1746808631422201857', '1746808631422201857', '2024-02-05 09:24:58', '2024-02-05 09:24:58', '1731567974296924163', 0, '1742806434684911618', 3, 4);
COMMIT;

-- ----------------------------
-- Table structure for sys_route_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_route_role`;
CREATE TABLE `sys_route_role` (
                                `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                                `role_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
                                `route_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路由ID',
                                `del` int NOT NULL COMMENT '数据删除标识：1正常，0删除',
                                `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                                `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                                `update_time` datetime NOT NULL COMMENT '更新时间',
                                `create_time` datetime NOT NULL COMMENT '创建时间',
                                `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID。租户ID',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-路由角色信息表';

-- ----------------------------
-- Records of sys_route_role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_sql
-- ----------------------------
DROP TABLE IF EXISTS `sys_sql`;
CREATE TABLE `sys_sql` (
                         `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                         `api_service_name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接口所在服务名称',
                         `create_time` datetime(6) NOT NULL COMMENT '创建时间',
                         `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建用户',
                         `del` int NOT NULL COMMENT '删除标识：1正常，0删除',
                         `update_time` datetime(6) NOT NULL COMMENT '更新时间',
                         `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新用户',
                         `req_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作用户的IP地址',
                         `req_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作的URI',
                         `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区域ID，租户ID',
                         `original_sql` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原始执行SQL',
                         `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'sql执行参数',
                         `execute_sql` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '最终执行sql',
                         `time` bigint DEFAULT NULL COMMENT 'sql执行耗时',
                         `original_time` bigint DEFAULT NULL COMMENT '配置sql执行时间',
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-全局日志记录表';

-- ----------------------------
-- Records of sys_sql
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_system_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_system_info`;
CREATE TABLE `sys_system_info` (
                                 `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                                 `server_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '服务注册nacos所在IP',
                                 `server_port` int NOT NULL COMMENT '服务注册nacos所在端口信息',
                                 `server_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '服务名称',
                                 `space_total` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '总共储存空间',
                                 `space_free` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '空闲储存空间',
                                 `space_usable` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '可用储存空间',
                                 `os_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '系统名称',
                                 `os_pid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '系统Pid',
                                 `os_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '系统用户名',
                                 `os_user_home` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '系统用户挂载路径',
                                 `java_home` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jdk路径',
                                 `java_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jdk版本信息',
                                 `jvm_memory_total` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jvm初始总内存',
                                 `jvm_memory_max_use` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jvm最大可用内存',
                                 `jvm_memory_used` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jvm已使用内存',
                                 `physical_memory_total` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '总共物理内存',
                                 `physical_memory_free` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '空闲物理内存',
                                 `cpu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cpu名称',
                                 `cpu_processor_count` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cpu核心数',
                                 `cpu_sys_used` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cpu系统使用率',
                                 `cpu_user_used` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cpu用户使用率',
                                 `cpu_wait` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cpu当前等待率',
                                 `cpu_free` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cpu当前空闲率',
                                 `cpu_used` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cpu总共利用率',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建数据时间',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-服务实例所在服务器基础信息表';

-- ----------------------------
-- Records of sys_system_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task` (
                          `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                          `job_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务名',
                          `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务描述',
                          `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cron表达式',
                          `bean_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务执行时调用哪个类的方法 包名+类名',
                          `job_status` tinyint DEFAULT NULL COMMENT '任务状态: 1暂停中，2执行中，3已删除',
                          `job_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务分组',
                          `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区域ID，租户ID',
                          `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新用户',
                          `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                          `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建用户',
                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                          `del` int DEFAULT NULL COMMENT '删除标识：1正常，0删除',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-定时任务信息';

-- ----------------------------
-- Records of sys_task
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_task_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_task_log`;
CREATE TABLE `sys_task_log` (
                              `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                              `job_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务名',
                              `job_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务id',
                              `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cron表达式',
                              `bean_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务执行时调用哪个类的方法 包名+类名',
                              `handle_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行IP',
                              `job_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务分组',
                              `handle_start_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行开始时间',
                              `handle_code` int DEFAULT NULL COMMENT '执行日志代码',
                              `handle_message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '执行异常信息',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-定时任务执行日志';

-- ----------------------------
-- Records of sys_task_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_info`;
CREATE TABLE `sys_tenant_info` (
                                 `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                                 `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域名称',
                                 `contact_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '负责人姓名',
                                 `contact_tel` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '负责人电话',
                                 `contact_user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '负责人id',
                                 `del` int NOT NULL COMMENT '数据删除标识：1正常，0删除',
                                 `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                                 `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                                 `update_time` datetime NOT NULL COMMENT '更新时间',
                                 `create_time` datetime NOT NULL COMMENT '创建时间',
                                 `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID。租户ID',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-区域信息数据表';

-- ----------------------------
-- Records of sys_tenant_info
-- ----------------------------
BEGIN;
INSERT INTO `sys_tenant_info` (`id`, `name`, `contact_name`, `contact_tel`, `contact_user_id`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`) VALUES ('1692461289663700995', '我的区域我做主', 'zhouxinxin', '17341787044', '1694531720272949249', 1, '1694531720272949249', '1694531720272949249', '2023-09-08 15:26:04', '2023-09-08 15:26:04', '1694531720272949250');
INSERT INTO `sys_tenant_info` (`id`, `name`, `contact_name`, `contact_tel`, `contact_user_id`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`) VALUES ('1700062931194601473', '我的区域我做主1', 'zhouxinxin', '17341787044', '1694531720272949249', 1, '1694531720272949249', '1694531720272949249', '2023-09-08 16:26:03', '2023-09-08 16:26:03', '1694531720272949250');
INSERT INTO `sys_tenant_info` (`id`, `name`, `contact_name`, `contact_tel`, `contact_user_id`, `del`, `update_user`, `create_user`, `update_time`, `create_time`, `data_tenant_id`) VALUES ('1731567974296924163', '华为园区', 'a-xin', '17341787777', '1731567974296924161', 1, '1731567974296924161', '1731567974296924161', '2023-12-04 14:55:51', '2023-12-04 14:55:51', '1731567974296924163');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_info`;
CREATE TABLE `sys_user_info` (
                               `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据ID',
                               `acesskey` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '加解密key',
                               `create_time` datetime(6) NOT NULL COMMENT '创建时间',
                               `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建用户',
                               `del` int NOT NULL COMMENT '删除标识：1正常，0删除',
                               `update_time` datetime(6) NOT NULL COMMENT '更新时间',
                               `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '更新用户',
                               `age` int DEFAULT NULL COMMENT '年龄',
                               `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
                               `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
                               `pass_word` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
                               `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
                               `data_tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域ID，租户ID',
                               `open_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '小程序用户openId',
                               `user_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户类型：1超管，0普通成员',
                               `regist_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户注册方式：1：web，2：app',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sys-用户信息表';

-- ----------------------------
-- Records of sys_user_info
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_info` (`id`, `acesskey`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `age`, `email`, `name`, `pass_word`, `phone`, `data_tenant_id`, `open_id`, `user_type`, `regist_type`) VALUES ('1731567974296924161', 'm3ydm0ncrzlax3fg', '2023-12-04 14:55:50.717000', '1731567974296924161', 1, '2023-12-11 11:12:23.165000', '1731567974296924161', 100, '11111111111111111111@qq.com', 'a-xin', 'ls4kmdMl4KCwxgQJzYjKCQ==', '17341787777', '1731567974296924163', '1', '1', '2');
INSERT INTO `sys_user_info` (`id`, `acesskey`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `age`, `email`, `name`, `pass_word`, `phone`, `data_tenant_id`, `open_id`, `user_type`, `regist_type`) VALUES ('1732324096997945346', 'ci9dvi2n7vw3yfsc', '2023-12-06 17:00:24.407000', '1732324096997945346', 1, '2023-12-06 17:00:24.407000', '1732324096997945346', 0, '111@123.com', 'xiao', 'XYejvDssfR6i4EUNu43MvQ==', '17341415151', '1731567974296924163', '2', '1', '2');
INSERT INTO `sys_user_info` (`id`, `acesskey`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `age`, `email`, `name`, `pass_word`, `phone`, `data_tenant_id`, `open_id`, `user_type`, `regist_type`) VALUES ('1746808631422201857', '1kg9z3tcku6ubbx8', '2024-01-15 16:16:46.000000', '1746808631422201857', 1, '2024-05-07 14:37:34.266000', '1746808631422201857', 0, '11111111111111111111@qq.com', 'web-01', 'fOFkeBF0M5amTbgm5Yb0Rg==', '17341789995', '1731567974296924163', 'openid-0002', '1', '2');
INSERT INTO `sys_user_info` (`id`, `acesskey`, `create_time`, `create_user`, `del`, `update_time`, `update_user`, `age`, `email`, `name`, `pass_word`, `phone`, `data_tenant_id`, `open_id`, `user_type`, `regist_type`) VALUES ('1747795778864771073', '6oa8g0nb0pqppgbj', '2024-01-18 09:39:20.828000', '1747795778864771073', 1, '2024-01-18 09:39:20.828000', '1747795778864771073', 10, '123@11.com', '前期去去去', 'CW+UwmezsNR8dyTUjVFVWQ==', '17341414141', '1731567974296924163', NULL, '1', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
