CREATE TABLE "gateway_log" (
  "id" varchar(255) NOT NULL,
  "api_service_name" varchar(16),
  "create_time" timestamp NOT NULL,
  "create_user" varchar(255),
  "route" varchar(255),
  "req_ip" varchar(255),
  "req_uri" varchar(255),
  "data_tenant_id" varchar(255),
  "req_method" varchar(255),
  "req_params" varchar(255),
  "resp_data" text,
  "time" int4,
  "resp_status" int4,
  "req_type" varchar(255),
  "req_headers" text,
  PRIMARY KEY ("id")
);
COMMENT ON COLUMN "gateway_log"."id" IS '数据ID';
COMMENT ON COLUMN "gateway_log"."api_service_name" IS '接口所在服务名称';
COMMENT ON COLUMN "gateway_log"."create_time" IS '创建时间';
COMMENT ON COLUMN "gateway_log"."create_user" IS '创建用户';
COMMENT ON COLUMN "gateway_log"."route" IS '路由信息';
COMMENT ON COLUMN "gateway_log"."req_ip" IS '操作用户的IP地址';
COMMENT ON COLUMN "gateway_log"."req_uri" IS '操作的URI';
COMMENT ON COLUMN "gateway_log"."data_tenant_id" IS '区域ID，租户ID';
COMMENT ON COLUMN "gateway_log"."req_method" IS '操作的方式';
COMMENT ON COLUMN "gateway_log"."req_params" IS '操作提交的数据';
COMMENT ON COLUMN "gateway_log"."resp_data" IS '返回的数据';
COMMENT ON COLUMN "gateway_log"."time" IS '耗时';
COMMENT ON COLUMN "gateway_log"."resp_status" IS '返回数据状态码';
COMMENT ON COLUMN "gateway_log"."req_type" IS '网关请求类型';
COMMENT ON COLUMN "gateway_log"."req_headers" IS '网关请求头部信息';
COMMENT ON TABLE "gateway_log" IS 'gateway-网关访问请求日志信息';

CREATE TABLE "sms_log" (
  "id" varchar(255) NOT NULL,
  "create_time" timestamp NOT NULL,
  "phone" varchar(16) NOT NULL,
  "ip_address" varchar(50) NOT NULL,
  "params" varchar(255) NOT NULL,
  "message" varchar(255) NOT NULL,
  "del" int2 NOT NULL,
  CONSTRAINT "_copy_2" PRIMARY KEY ("id")
);
COMMENT ON COLUMN "sms_log"."id" IS '数据ID';
COMMENT ON COLUMN "sms_log"."create_time" IS '创建时间';
COMMENT ON COLUMN "sms_log"."phone" IS '下发手机号';
COMMENT ON COLUMN "sms_log"."ip_address" IS '请求IP地址信息';
COMMENT ON COLUMN "sms_log"."params" IS '参数信息';
COMMENT ON COLUMN "sms_log"."message" IS '短信模板信息';
COMMENT ON COLUMN "sms_log"."del" IS '是否删除';
COMMENT ON TABLE "sms_log" IS '短信下发日志记录';

CREATE TABLE "sys_log" (
  "id" varchar(255) NOT NULL,
  "api_service_name" varchar(16),
  "create_time" timestamp NOT NULL,
  "create_user" varchar(255),
  "update_time" timestamp NOT NULL,
  "update_user" varchar(255),
  "api_class" varchar(255),
  "api_method" varchar(255),
  "log_title" varchar(255),
  "req_ip" varchar(255),
  "req_uri" varchar(255),
  "data_tenant_id" varchar(255),
  "req_method" varchar(255),
  "req_params" text,
  "resp_data" text,
  "exception" text,
  "time" int8,
  "resp_status" int4,
  "req_type" varchar(255),
  CONSTRAINT "_copy_1" PRIMARY KEY ("id")
);
COMMENT ON COLUMN "sys_log"."id" IS '数据ID';
COMMENT ON COLUMN "sys_log"."api_service_name" IS '接口所在服务名称';
COMMENT ON COLUMN "sys_log"."create_time" IS '创建时间';
COMMENT ON COLUMN "sys_log"."create_user" IS '创建用户';
COMMENT ON COLUMN "sys_log"."update_time" IS '更新时间';
COMMENT ON COLUMN "sys_log"."update_user" IS '更新用户';
COMMENT ON COLUMN "sys_log"."api_class" IS '调用的controller全类名';
COMMENT ON COLUMN "sys_log"."api_method" IS '调用的controller中的方法';
COMMENT ON COLUMN "sys_log"."log_title" IS '日志标题';
COMMENT ON COLUMN "sys_log"."req_ip" IS '操作用户的IP地址';
COMMENT ON COLUMN "sys_log"."req_uri" IS '操作的URI';
COMMENT ON COLUMN "sys_log"."data_tenant_id" IS '区域ID，租户ID';
COMMENT ON COLUMN "sys_log"."req_method" IS '操作的方式';
COMMENT ON COLUMN "sys_log"."req_params" IS '操作提交的数据';
COMMENT ON COLUMN "sys_log"."resp_data" IS '返回的数据';
COMMENT ON COLUMN "sys_log"."exception" IS '异常信息';
COMMENT ON COLUMN "sys_log"."time" IS '耗时';
COMMENT ON COLUMN "sys_log"."resp_status" IS '返回数据状态码';
COMMENT ON COLUMN "sys_log"."req_type" IS '访问方式';
COMMENT ON TABLE "sys_log" IS 'sys-全局日志记录表';

