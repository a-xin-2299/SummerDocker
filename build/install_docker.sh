#！/bin/bash

#移除老版本
yum remove docker \
	docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine

#安装必要的系统工具
yum install -y yum-utils device-mapper-persistent-data lvm2

#添加软件源信息
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

#更新yum缓存
yum makecache fast

#安装docker-ce
yum -y install docker-ce

#开机自启
systemctl enable docker

#启动dicker
systemctl start docker

