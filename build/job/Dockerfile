FROM openjdk:8

MAINTAINER a-xin <1846221495@qq.com>
VOLUME /tmp

# 设置jar包激活环境
ENV PROFILE="prod"
ENV JAR_NAME="SummerJob-1.0.0.jar"

ENV BASE_DIR="/usr/local"
RUN mkdir -p ${BASE_DIR}/logs/jar

# 激活配置文件
ENV ACTIVE="-Dspring.profiles.active=${PROFILE}"
# 日志文件
ENV JAVA_LOG="--logging.file=${BASE_DIR}/logs/jar/${JAR_NAME}.log"
# 设置jvm参数大小
ENV JVM="-Xms1024m -Xmx2048m -Xmn1024m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=256m"
# OOM异常 堆转储
ENV OOM="-XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${BASE_DIR}/logs/${JAR_NAME}_heapdump.hprof"
# GC相关参数
ENV GC="-XX:+UseG1GC -XX:ParallelGCThreads=4 -XX:ConcGCThreads=2 -XX:MaxGCPauseMillis=200"
# GC日志打印
ENV GC_LOG="-Xloggc:${BASE_DIR}/logs/${JAR_NAME}_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M"

#设置环境参数
ENV JAVA_OPTS="${JVM} ${OOM} ${GC} ${GC_LOG} ${ACTIVE} ${JAVA_LOG}"
# 设置环境变量，例如时区
ENV TZ=Asia/Shanghai

#复制jar包到镜像中，并且将名字改成app.jar
ADD ./jar/${JAR_NAME} ${BASE_DIR}/app.jar
# 工作路径
WORKDIR ${BASE_DIR}
# 映射端口
EXPOSE 9093

# ENTRYPOINT 或 CMD 只会执行一次 , 会覆盖之前的 ENTRYPOINT 或 CMD 命令
ENTRYPOINT ["java", "-jar", "app.jar", "${JAVA_OPTS}"]
